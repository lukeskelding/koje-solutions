<?php


class KojeSolutions_GridFieldOrderableRows extends GridFieldOrderableRows {
    
    public function getManipulatedData(GridField $grid, SS_List $list) {
		$state = $grid->getState();
		$sorted = (bool) ((string) $state->GridFieldSortableHeader->SortColumn);

		$state->GridFieldOrderableRows->enabled = !$sorted;

		if(!$sorted) {
			$sortterm = '';
			if ($this->extraSortFields) {
				if (is_array($this->extraSortFields)) {
					foreach($this->extraSortFields as $col => $dir) {
						$sortterm .= "$col $dir, ";
					}
				} else {
					$sortterm = $this->extraSortFields.', ';
				}
			}
			$sortterm .= $this->getSortTable($list).'.'.$this->getSortField();
			return $list->sort($sortterm);
		} else {
			return $list;
		}
	}
}