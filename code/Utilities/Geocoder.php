<?php


class Geocoder extends Object {

    private static $api_key = '';

    public static function geocode($address, $coords_only = true) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, sprintf(
            'https://maps.googleapis.com/maps/api/geocode/json?key=%s&address=%s&sensor=false',
            Config::inst()->get('Geocoder', 'api_key'),
            urlencode($address)
        ));
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $response = json_decode(curl_exec($curl), false);
        if ($response->status == 'OK' && count($response->results) > 0){
            return $coords_only ? $response->results[0]->geometry->location : $response->results[0];
        }
        return null;
    }

    public static function reverse($latitude, $longitude, $types = array('country')) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, sprintf(
            'https://maps.googleapis.com/maps/api/geocode/json?key=%s&latlng=%f,%f&types=%s',
            Config::inst()->get('Geocoder', 'api_key'),
            $latitude,
            $longitude,
            implode('|', $types)
        ));
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $response = json_decode(curl_exec($curl), false);
        if ($response->status == 'OK' && count($response->results) > 0) {
            return $response->results[0]->address_components[0]->long_name;
        }
        return null;
    }
}
