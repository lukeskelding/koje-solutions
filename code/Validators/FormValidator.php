<?php


class FormValidator extends Validator {
    
    private $_rules;
    
    public function __construct($rules = array()) {
        $this->_rules = $rules;
        parent::__construct();
    }
    
    public function setForm($form) {
        parent::setForm($form);
        $form->setAttribute('novalidate', 'novalidate');
        return $this;
    }
    
    public function php($data) {
		
		
        foreach ($this->_rules as $field => $checks) {
            $valid = true;
            if (array_key_exists($field, $data)) {
                $value = $data[$field];
                $value_valid = true;
                if (count($checks) > 0) {
                    
                    $methods = $checks;
                    $messages = null;
                    
                    $keys = array_keys($checks);
                    if (is_string($keys[0])) {
                        $methods = $keys;
                        $messages = $checks;
                    }
                    
                    foreach ($methods as $signature) {
						$args = array();
						if (preg_match('/^([a-zA-Z0-9\_]*)\((.*?)\)$/', $signature, $match)) {
							$method = $match[1];
							$args = array_map('trim', explode(',', $match[2]));
						}
						else {
							$method = $signature;
						}
						
                        $value_valid = $this->$method($value, $value_valid, $this->form->Fields()->dataFieldByName($field), $args);
                        if (!$value_valid) {
                            $valid = false;
                            if (is_null($this->errors)) {
                                $this->errors = array();
                            }
                            $this->errors[] = array(
                                'fieldName' => $field,
                                'message' => is_null($messages) ? '' : $messages[$signature],
                                'messageType' => $method
                            );
                        }
                    }
                }
            }
        }
    }
    
    public function fieldIsRequired($name) {
        if (array_key_exists($name, $this->_rules)) {
            $keys = array_keys($this->_rules[$name]);
            if (is_string($keys[0])) {
                return array_key_exists('required', $this->_rules[$name]);
            }
            else {
                return in_array('required', $this->_rules[$name]);
            }
        }
        return false;
    }
    
    public function required($value, $valid, $field){
		if (is_null($value)){
			return false;
		}
		elseif (is_string($value)){
			$value = trim($value);
			return (strlen($value) > 0);
		}
		elseif (is_array($value) && array_key_exists('size', $value)) {
    		return $value['size'] > 0;
		}
		else {
			return true;
		}
    }
    
    public function email_address($value, $valid, $field){
		if (strlen($value) > 0){
            return preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $value);
        }
        else {
            return true;
        }
    }
	
	public function mx_records($value, $valid, $field){
		if ($valid){
			return getmxrr(substr($value, strripos($value, '@') + 1), $hosts);
		}
		return true;
	}
	
	public function number($value, $valid, $field){
		return ($valid && strlen($value) > 0) ? is_numeric($value) : true;
	}
	
	public function number_integer($value, $valid, $field){
		return ($valid && strlen($value) > 0) ? ctype_digit($value) : true;
	}
    
    public function phone_number($value, $valid, $field){
        if (strlen($value) > 0){
            return eregi("^[0-9]{11}$", $value);
        }
        else {
            return true;
        }
    }
}