<?php


class ThemedEmail extends StyledHtmlEmail {
    
    public function populateTemplate($data) {
        $this->setTemplate('ThemedEmail');
        $body = new SSViewer(get_class($this));
        parent::populateTemplate(new ArrayData(array(
            'Body' => $body->process($data)
        )));
    }
}