<?php

class SlugModel extends DataObject {
	
    private static $slugged_field = 'Name';
	private static $db = array(
		'Slug' => 'Varchar(100)'
	);
	private static $indexes = array();

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->removeFieldFromTab('Root.Main', 'Slug');
        return $fields;
	}
	
	public function onBeforeWrite() {
		parent::onBeforeWrite();
        $slugged = $this->stat('slugged_field');
        if (is_array($slugged)) {
            $slug = array();
            foreach ($slugged as $field) {
                $slug[] = $this->$field;
            }
            $slug = implode(' ', $slug);
        }
        else {
            $slug = $this->$slugged;
        }
		$this->Slug = StringHelpers::form_slug($slug);
	}
}