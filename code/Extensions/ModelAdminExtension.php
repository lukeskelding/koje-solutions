<?php


class ModelAdminExtension extends Extension {
    
    public function getExportFields() {
        return singleton($this->owner->modelClass)->exportFields();
    }
}