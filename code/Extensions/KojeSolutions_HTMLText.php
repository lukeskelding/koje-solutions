<?php


class KojeSolutions_HTMLText extends Extension {
    
    public function NoParagraphs() {
        $html = HTMLText::create();
        $html->setValue(preg_replace('/<p\b[^>]*>/i', '', preg_replace('/<\/p>/i', '', $this->owner->value)));
        return $html;
    }
}