<?php


class DecimalExtension extends Extension {

	public function DynamicDecimal() {
		return abs($this->owner->value);
	}
	
	public function Currency() {
        return sprintf('&pound;%s', number_format($this->owner->value, 2));
    }
}