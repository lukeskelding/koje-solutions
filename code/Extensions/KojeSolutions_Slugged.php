<?php

class KojeSolutions_Slugged extends DataExtension {
	
    private static $slugged_field = array();
	private static $db = array(
		'Slug' => 'Varchar(100)'
	);

	public function updateCMSFields(FieldList $fields) {
		$fields->removeFieldFromTab('Root.Main', 'Slug');
	}
	
	public function onBeforeWrite() {
		$slugged = $this->owner->stat('slugged_field');
		if (!empty($slugged) && !is_null($slugged)) {
    		$slug = array();
            foreach ($slugged as $field) {
                $slug[] = $this->owner->$field;
            }
            $slug = trim(implode(' ', $slug));
            $this->owner->Slug = StringHelpers::form_slug($slug);
		}
	}
}