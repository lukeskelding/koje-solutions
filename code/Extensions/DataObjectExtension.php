<?php


class DataObjectExtension extends Extension {
    
    private static $export_fields = array();
    
    public function exportFields() {
        $fields = $this->owner->stat('export_fields');
        if (count($fields) < 1) {
            $fields = $this->owner->stat('summary_fields');
        }
        return $fields;
    }
}