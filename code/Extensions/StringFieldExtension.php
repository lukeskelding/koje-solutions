<?php


class StringFieldExtension extends Extension {
	
	public function Slugify($separator = '-') {
		return StringHelpers::form_slug($this->owner->value, $separator);
	}
	
	public function LineBreaks() {
    	$html = HTMLText::create();
    	$html->setValue(nl2br(Convert::raw2xml($this->owner), true));
    	return $html;
	}
	
	public function Lines() {
    	$lines = ArrayList::create();
    	foreach (explode("\n", $this->owner->value) as $line) {
        	$text = Text::create();
        	$text->setValue($line);
        	$lines->push($text);
    	}
    	return $lines;
	}
	
	public function BreakAfter($count) {
    	$words = preg_split('/\s+/', $this->owner->value);
    	$before = array_slice($words, 0, $count);
        $after = array_slice($words, $count);
    	$text = HTMLText::create();
    	$text->setValue(sprintf('%s<br />%s', implode(' ', $before), implode(' ', $after)));
    	return $text;
	}
}