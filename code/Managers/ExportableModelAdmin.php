<?php


class ExportableModelAdmin extends ModelAdmin {
    
    public function getExportFields() {
        return singleton($this->owner->modelClass)->exportFields();
    }
    
    public function import($data, $form, $request) {
		if(!$this->showImportForm || (is_array($this->showImportForm) 
				&& !in_array($this->modelClass,$this->showImportForm))) {

			return false;
		}

		$importers = $this->getModelImporters();
		$loader = $importers[$this->modelClass];

		if(
			empty($_FILES['_CsvFile']['tmp_name']) ||
			file_get_contents($_FILES['_CsvFile']['tmp_name']) == ''
		) {
			$form->sessionMessage(_t('ModelAdmin.NOCSVFILE', 'Please browse for a CSV file to import'), 'good');
			$this->redirectBack();
			return false;
		}

		if (!empty($data['EmptyBeforeImport']) && $data['EmptyBeforeImport']) { //clear database before import
			$loader->deleteExistingRecords = true;
		}
		$results = $loader->load($_FILES['_CsvFile']['tmp_name']);

		$message = '';
		if($results->CreatedCount()) $message .= _t(
			'ModelAdmin.IMPORTEDRECORDS', "Imported {count} records.",
			array('count' => $results->CreatedCount())
		);
		if($results->UpdatedCount()) $message .= _t(
			'ModelAdmin.UPDATEDRECORDS', "Updated {count} records.",
			array('count' => $results->UpdatedCount())
		);
		if($results->DeletedCount()) $message .= _t(
			'ModelAdmin.DELETEDRECORDS', "Deleted {count} records.",
			array('count' => $results->DeletedCount())
		);
		if ($results->SkippedCount()) {
    		$message .= sprintf('Skipped %d records.', $results->SkippedCount());
        }
		if(!$results->CreatedCount() && !$results->UpdatedCount()) {
			$message .= _t('ModelAdmin.NOIMPORT', "Nothing to import");
		}

		$form->sessionMessage($message, 'good');
		$this->redirectBack();
	}
}