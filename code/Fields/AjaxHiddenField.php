<?php


class AjaxHiddenField extends HiddenField {
    
    public static function create() {
        $args = func_get_args();
        $name = $args[0];
        $title = (count($args) > 1) ? $args[1] : null;
        $value = (count($args) > 2) ? $args[2] : null;
        return new AjaxHiddenField($name, $title, $value);
    }
    
    public function IsHidden() {
        return false;
    }
}