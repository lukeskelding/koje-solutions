<?php


class RichTextareaField extends TextareaField {

    public function __construct($name, $title = null, $value = null) {
        parent::__construct($name, $title, $value);
        $module = 'kojesolutions';
        Requirements::javascript(sprintf('%s/thirdparty/tiny_mce/tiny_mce.js', $module));
        Requirements::javascript(sprintf('%s/thirdparty/tiny_mce/jquery.tinymce.js', $module));
		Requirements::javascript(sprintf('%s/javascript/%s.js', $module, get_class($this)));
    }

    public function deferLoading() {
        return $this->setAttribute('data-deferred', 'true');
    }
}
