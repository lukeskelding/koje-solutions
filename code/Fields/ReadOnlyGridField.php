<?php


class ReadOnlyGridField extends GridField {
    
    public function __construct($name, $title, $source) {
        parent::__construct($name, $title, $source, GridFieldConfig_Base::create());
        $this->getConfig()->removeComponentsByType('GridFieldFilterHeader');
    }
    
    public function performReadonlyTransformation() {
        return $this;
    }
}