<?php


class TimeDropdownField extends TimeField {
    
    private $_children;
    
    public function __construct($name, $title = null, $value = "") {
        $hours = array();
        for ($i=0; $i<24; $i++) {
            $hour = str_pad(sprintf('%d', $i), 2, '0', STR_PAD_LEFT);
            $hours[$hour] = $hour;
        }
        $minutes = array();
        for ($i=0; $i<60; $i+=15) {
            $minute = str_pad(sprintf('%d', $i), 2, '0', STR_PAD_LEFT);
            $minutes[$minute] = $minute;
        }
		$this->_children = new FieldList(
			new DropdownField(
				"{$name}[_Hour]", 
				'Hour',
				$hours
			),
			new DropdownField(
				"{$name}[_Minute]", 
				'Minute',
				$minutes
			)
		);
		
		parent::__construct($name, $title, $value);
	}
	
	public function Field($properties = array()) {
		$content = array();
		
		foreach ($this->_children as $field) {
			$field->setDisabled($this->isDisabled()); 
			$field->setReadonly($this->isReadonly());

			if (count($this->attributes)) {
				foreach ($this->attributes as $name => $value) {
					$field->setAttribute($name, $value);
				}
			}

			$content[] = $field->Field();
		}

		return implode('<span>:</span>', $content);
	}
	
	public function setValue($value, $data = null) {
    	$hour = $minute = null;
    	$current = $this->value;
		if ($current != $value) {
    		if (is_string($value) && preg_match('/^([0-9]{2})\:([0-9]{2})/', $value, $matches)) {
        		$hour = $matches[1];
                $minute = $matches[2];
    		}
    		else if (is_array($value)) {
        		$hour = $value['_Hour'];
        		$minute = $value['_Minute'];
    		}
		}
		if (!is_null($hour)) {
    		$this->_children->fieldByName($this->getName() . '[_Hour]')->setValue($hour);
		}
		if (!is_null($minute)) {
    		$this->_children->fieldByName($this->getName() . '[_Minute]')->setValue($minute);
		}
		$this->value = $value;
		return $this;
	}
	
	public function dataValue() {
    	$value = '';
    	if (is_array($this->value)) {
    	    $value = implode(':', array_values($this->value));
        }
    	return (strlen($value) > 1) ? $value : null;
	}
}