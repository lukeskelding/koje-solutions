<?php


class StringHelpers {
    
    public static function form_slug($value, $separator = '-') {
		$slug = str_replace(' ', $separator, strtolower($value));
        $slug = str_replace('&', 'and', $slug);
        $slug = preg_replace('/[^a-z0-9\\' . $separator . ']?/', '', $slug);
        while (stripos($slug, $separator . $separator) !== false) {
            $slug = str_replace($separator . $separator, $separator, $slug);
        }
        return $slug;
	}
}