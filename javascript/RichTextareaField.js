$(window).load(function(){
    loadRichTextareaField($('form div.richtextarea > textarea:not([data-deferred=true])'));
});

function loadRichTextareaField(elements) {
    $(elements).tinymce({
        script_url : '../../tiny_mce/tiny_mce.js',
        theme : "advanced",
        plugins : "paste",
        theme_advanced_blockformats : "p,h2,h3,h4",
        paste_auto_cleanup_on_paste : false,
        extended_valid_elements : "span[!class]",
        editor_deselector : "preview",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,removeformat,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|cut,copy,paste",
        theme_advanced_buttons2 : "undo,redo,|,link,unlink,|,formatselect,styleselect,|,code",
        theme_advanced_buttons3 : "",
        theme_advanced_buttons4 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        relative_urls : false,
        formats : {
            alignleft : {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'align_left'},
            aligncenter : {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'align_centre'},
            alignright : {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'align_right'},
            alignfull : {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'justify'},
            bold : {inline : 'span', classes : 'bold'},
            italic : {inline : 'span', classes : 'italic'},
            underline : {inline : 'span', classes : 'underline'},
            strikethrough : {inline : 'span', classes : 'strikethrough'}
        },
        style_formats : [
            {title : 'Red', inline : 'span', classes : 'red'},
        ]
    });
}
